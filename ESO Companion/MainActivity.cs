﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Graphics;

namespace ESO_Companion
{
    [Activity(Label = "ESO Companion", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        int count = 1;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetTheme(Resource.Style.Theme_Custom);
            Typeface tf = Typeface.CreateFromAsset(Assets, "fonts/samplefont.ttf");
            TextView textConnecting = FindViewById<TextView>(Resource.Id.textConnecting);
            
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button button = FindViewById<Button>(Resource.Id.MyButton);

            button.Click += delegate { button.Text = string.Format("{0} clicks!", count++); };

            MainThread thread = new MainThread(this);
            thread.Start();
        }
    }
}

